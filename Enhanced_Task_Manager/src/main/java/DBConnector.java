import java.sql.*;

public class DBConnector {
    private static Connection connection;

    public DBConnector() throws ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    public Connection getConnection(String url, String username, String password) throws SQLException, ClassNotFoundException {
        if (connection != null)
            return connection;
        connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    static Connection ConnectDB() throws ClassNotFoundException, SQLException {
        final String USERNAME = "root";
        final String PASSWORD = "root";
        final String URL = "jdbc:mysql://localhost:3306/task_manager";
        DBConnector db = new DBConnector();
        Connection myconnection = db.getConnection(URL, USERNAME, PASSWORD);
        return myconnection;
    }
}

