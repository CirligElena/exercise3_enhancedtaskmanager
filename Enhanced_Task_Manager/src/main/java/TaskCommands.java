import java.io.*;
import java.sql.*;

public class TaskCommands {
    static File f = new File("C:\\Users\\User\\IdeaProjects\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");

    public static void addTask(String insert_un, String task_title, String task_description, String group_name) throws IOException, ClassNotFoundException, SQLException {
        Task task = new Task();
        task.setInsert_un(insert_un);
        task.setTask_title(task_title);
        task.setTask_description(task_description);
        task.setGroup_name(group_name);
        Connection conn = DBConnector.ConnectDB();
        String insert = "insert into tasks (user_name, task_title, task_description, group_name) values(?, ?, ?, ?)";
        PreparedStatement prepInsert = conn.prepareStatement(insert);
        prepInsert.setString(1, task.getInsert_un());
        prepInsert.setString(2, task.getTask_title());
        prepInsert.setString(3, task.getTask_description());
        prepInsert.setString(4, task.getGroup_name());
        prepInsert.execute();

    }

    public static void showTasks() throws IOException, ClassNotFoundException, SQLException {
        Connection conn = DBConnector.ConnectDB();
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("select*from tasks");
        while (resultSet.next()) {
            Task task = new Task();
            task.setInsert_un(resultSet.getString(1));
            task.setTask_title(resultSet.getString(2));
            task.setTask_description(resultSet.getString(3));
            task.setGroup_name(resultSet.getString(4));
            System.out.println(task);

        }
    }


}
