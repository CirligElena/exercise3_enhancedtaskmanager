import java.io.*;
import java.sql.*;

public class UserCommands {

    public static void createUser(String fname, String lname, String uname) throws IOException, SQLException, ClassNotFoundException {
        User user = new User();
        user.setFirst_name(fname);
        user.setLast_name(lname);
        user.setUser_name(uname);
        Connection conn = DBConnector.ConnectDB();
        String insert = "insert into users (first_name, last_name, user_name) values(?, ?, ?)";
        PreparedStatement prepInsert = conn.prepareStatement(insert);
        prepInsert.setString(1, user.getFirst_name());
        prepInsert.setString(2, user.getLast_name());
        prepInsert.setString(3, user.getUser_name());
        prepInsert.execute();
    }


    public static void showAllUsers() throws ClassNotFoundException, SQLException {
        Connection conn = DBConnector.ConnectDB();
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery("select*from users");
        while (resultSet.next()) {
            User user = new User();
            user.setFirst_name(resultSet.getString(1));
            user.setLast_name(resultSet.getString(2));
            user.setUser_name(resultSet.getString(3));
            System.out.println(user);

        }

    }
}


