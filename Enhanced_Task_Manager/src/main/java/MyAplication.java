import java.io.IOException;
import java.sql.SQLException;

public class MyAplication {
    public static void main(String [] args) throws IOException, ClassNotFoundException, SQLException {
       String method = args[0];

     if(method.equals("createUser")) {
         String fn = args[1];
         String ln = args[2];
         String un = args[3];
         UserCommands.createUser(fn, ln, un);
     } else if (method.equals("showAllUsers")) {
            UserCommands.showAllUsers();
        } else if (method.equals("addTask")) {
         String un = args[1];
         String tt = args[2];
         String td = args[3];
         String gn = args[4];
         TaskCommands.addTask(un, tt, td, gn);
     }else if (method.equals("showTasks")){
         TaskCommands.showTasks();
     }else System.out.println("Such command doesn't exist");

    }
}